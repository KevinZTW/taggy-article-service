import "./config.js";
import "./app/models/firebaseconfig.js";
import express from "express";
export const index = express();
import { dirname } from "path";
import { fileURLToPath } from "url";
import bodyParser from "body-parser";
import cors from "cors";
import { article_router } from "./app/routes/article_route.js";
import { rss_router } from "./app/routes/rss_route.js";
import { user_router } from "./app/routes/user_route.js";
import { loopAndFetchRSS } from "./app/service/fetchRSS.js";

const __dirname = dirname(fileURLToPath(import.meta.url));
const port = process.env.PORT || 3000;

index.use(cors());
index.use(bodyParser.urlencoded({ extended: false }));
index.use(bodyParser.json());

index.use("/route", [article_router, rss_router, user_router]);

setInterval(loopAndFetchRSS, 43200000); //half day

// Page not found
index.use(function (req, res, next) {
  res.status(404).send("404 This is not the web page you are looking for");
});

// Error handling
index.use(function (req, res, next) {
  res.status(500).send("Internal Server Error");
});

index.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
